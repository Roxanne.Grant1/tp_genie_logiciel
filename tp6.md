# TD6 : Création du Domain Model

Maintenant que vous avez vos user stories ainsi qu'une architecture nous allons créer le domain model.

## C'est quoi le Domain Model

Le domain model représente le modèle des objets de votre projet. Si vous deviez enlever toutes les interfaces utilisateur, la logique applicative et les controller et qu'il ne resterait plus qu'une console et un serveur SQL, à quoi cela ressemblerait-il? Contrairement au logical view, le domain model est plus précis au niveau des attributs et ne contient pas de méthode. Il permet de montrer spécifiquement qu'elles sont les entités de votre projet, leurs attributs ainsi que leurs relations.

# Remarques

* Votre domain model peut être soit un diagramme de classes soit un modèle entité-relation.
* Votre domain model doit être précis contrairement au logical view où vous avez fait un diagramme de classes de plus haut niveau.
* Comme rien n'est jamais figé, lors de votre implémentation cela peut encore changer, mais faite déjà au mieux.
* Soyez précis avec la nomenclature UML!

## A rendre

Avant le prochain cours, un document pdf par groupes contenant un graphique de votre domain model.

## Quelques ressources pour vous aider

* Un exemple d'un domain model pour la gestion d'un hôpital: https://www.uml-diagrams.org/examples/hospital-domain-diagram.html

* Ce power-point revient plus en détail sur le domain model ainsi que sur les règles UML par apport au diagramme de classes si vous les avez oubliées: http://homepage.divms.uiowa.edu/~tinelli/classes/022/Spring15/Notes/chap9.pdf
