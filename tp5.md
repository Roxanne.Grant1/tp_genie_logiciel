# TD 5 : Architecture Logicielles

Le but de ce TD est de mieux comprendre le but et fonctionnement de différentes architectures logicielles.
Pour ce faire vous devez réaliser un schéma de l'architecture de votre projet qui doit implémenter une architecture Micro-service, MVC et Layered. Pour ce faire quelques points importants:

* Il n'est pas nécessaire de faire un exemple trop précis avec trop de fonctionnalités, l'important est de démontrer que vous avez compris les concepts.

* Pour la partie micro-services, essayez de décomposer vos fonctionnalités en service les plus fonctionnellement atomiques c'est-à-dire que chaque service doit être indépendant et représenter une fonctionnalité propre.

* N'oubliez pas que la partie MVC s'applique simplement à l'interface utilisateur.

* Une architecture Layered doit avoir aux minimums deux couches.

## A rendre
Avant le prochain cours, un document pdf contenant un graphique des composants de votre architecture.


## Quelques ressources pour vous aider

* Architecture micro-service : https://openclassrooms.com/fr/courses/4668056-construisez-des-microservices/5122300-apprehendez-larchitecture-microservices

* Architecture MVC: https://practicalprogramming.fr/mvc/

* Ce livre de o'reillys sur les architectures logicielles est parfait pour vous :
https://www.oreilly.com/programming/free/files/software-architecture-patterns.pdf


