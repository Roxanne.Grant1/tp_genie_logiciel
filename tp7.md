# TD 7 : Design de votre API REST

Le but de ce TD est de concevoir votre API REST pour les quatre uses cases principales de votre projet. Pour réaliser ceci il faut:

* Définir le verbe REST utilisé pour chaque requête.
* Définir les routes avec la bonne nomenclature.
* Expliquer les valeurs de retours et les valeurs envoyées s'il y en a.
* Donner le code de retour de la requête.

## Exemple

| VERB | URL       | BODY                        | RETURN CODE   |
|------|-----------|-----------------------------|---------------|
| POST | /products | {nom: aspirateur, prix: 23} | 201 (Created) |

## A rendre
Avant le prochain cours, un document pdf contenant le design de votre API REST.


## Quelques ressources pour vous aider

Il existe une tonne de ressources sur le net pour les API REST en voici quelques-unes :

* https://www.restapitutorial.com/

* https://openclassrooms.com/fr/courses/6573181-adoptez-les-api-rest-pour-vos-projets-web?archived-source=3449001


