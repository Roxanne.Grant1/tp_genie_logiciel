# Design pattern II

Ce deuxième TD sur les designs patterns va vous permettre d'implémenter deux autres design patterns, le template et le command. Comme pour la dernière fois, l'idée est d'implémenter ces patterns dans le contexte de votre projet. 
D'abord il s'agit de déterminer où il est judicieux de les utiliser et ensuite de les implémenter correctement. Pour pouvoir les utiliser de manière judicieuse voici quelques exemples qui pourraient vous aider:

Pour le Template pattern:
Il permet de décrire la structure de base ou le "squelette" d'un algorithme, sans définir explicitement la logique de chaque étape:

* Il est souvent utilisé quand on a beaucoup de ligne de code qui sont copier-coller car l'agorithme à plusieurs cas qui diverge juste un peu les uns des autres.

* Il est souvent utilisé pour des algorithmes qui ont plusieurs actions qui se suivent.

Pour le Command pattern:
Il permet d'encapsuler dans un objet toutes les données qui doivent être utilisé pour réaliser une action:
* Il permet d'implémenter une architecture producteur-consommateur.
* Il permet d'avoir une file d'attente des actions à réaliser et permet de gérer l'annulation d'une action.

# A rendre
Avant le 01/12/20, une archive contenant votre code source java pour les deux designs patterns ainsi qu'un bout de texte justifiant votre choix.

## Quelques ressources pour vous aider

* https://sourcemaking.com/design_patterns/template_method
* https://refactoring.guru/design-patterns/command
* https://medium.com/elp-2018/command-design-pattern-quest-ce-78bc93fb0942
* https://www.baeldung.com/java-command-pattern



Il y en a bien sûr plein d'autres...

