# TP 1 : Création des sprints de votre projet  

Le but de ce TP est d'utiliser les user stories que vous avez réalisées la dernière fois pour planifier vos sprints. Voici les tâches à réaliser:

1. Définir la durée des sprints (entre 2 et 4 semaines) ainsi que les jours/développeur-euse disponibles pour chacun d'entre eux.

2. Accorder la priorité aux user story en fonction de leur business value.

3. Créer une relation entre les story points et les jours/développeur-euse. Ex: 2 story points est égal à 1/2 jour/développeur-euse de travail.

4. Définir en équipe l'estimation de chaque story avec des story points en utilisant le SCRUM Poker.

5. Répartir vos user stories dans les différents sprints en prenant en compte la vélocité de l'équipe et la business value des story.

6. Planifier votre premier sprint et créer le sprint backlog pour celui-ci contenant les stories ainsi que leur décomposition en tâches techniques respectives.

## A rendre
 avant le 20/10/2020 (dans 2 semaines) dans un fichier pdf, 2 tableau représentant:
* Le product backlog contenant :
  * la story
  * la business value
  * le numéro du sprint la contenant
  * le story point
  * les jours/développeur-euse.

* Le sprint backlog du premier sprint contenant:
  * la story
  * les tâches techniques
  * le story point
  * les jours/développeur-euse.



## Remarques

* Tous les sprints doivent avoir la même durée.
* Faites bien attention à la priorité que vous accordez aux story, réalisez les stories qui sont les plus importantes pour le ou la client-e d'abord.

* L'estimation grâce au scrum poker doit être unanime. Si une personne n'est pas d'accord, prenez le temps d'argumenter votre position. Ex:  Pourquoi vous estimez cette story à une certaine valeur ?

* Diviser vos user stories en tâches techniques les plus unitaires possibles. Ex: "Réaliser le login" n'est pas une tâche technique par contre "Réaliser le template de la page de connexion" en est une.